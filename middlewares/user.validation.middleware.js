const { user } = require('../models/user');
const createUserValid = (req, res, next) => {
    // TODO: Implement validatior for user entity during creation

    const bodyKeys = Object.keys(req.body);

    let userKeys = Object.keys(user);
    userKeys.shift();

    let isAllProperty = true;
    let isAllPropertyFilled = true;

    for (let i = 0; i < userKeys.length; i++) {
        if (userKeys.findIndex(el => {
            return el === bodyKeys[i];
        }) === -1) {
            isAllProperty = false;
        }
    }

    if (isAllProperty) {
        if (userKeys.findIndex(el => {
            return req.body[el].trim() === '';
        }) > -1) {
            isAllPropertyFilled = false;
        }
    }

    if (req.body.id) {
        res.status(400)
        next(new Error('Not to need enter user id'));
    } else if (
        req &&
        req.body &&
        isAllProperty &&
        isAllPropertyFilled) {
        if (
            /([a-zA-Z0-9])*@gmail\.com$/.test(req.body.email) &&
            /\+380([0-9]){9}$/.test(req.body.phoneNumber) &&
            req.body.password.length >= 3 &&
            !req.body.password.includes(' ')
        ) {
            res.status(200);
            next();
        } else {
            res.status(400)
            next(new Error('User entity to create is not valid'));
        }
    } else {
        res.status(400)
        next(new Error('All fields for user entity to create is not filled'));
    }
}

const updateUserValid = (req, res, next) => {
    // TODO: Implement validatior for user entity during update

    const bodyKeys = Object.keys(req.body);

    let userKeys = Object.keys(user);
    userKeys.shift();

    let isAllProperty = true;
    let isAllPropertyFilled = true;

    for (let i = 0; i < userKeys.length; i++) {
        if (userKeys.findIndex(el => {
            return el === bodyKeys[i];
        }) === -1) {
            isAllProperty = false;
        }
    }

    if (isAllProperty) {
        if (userKeys.findIndex(el => {
            return req.body[el].trim() === '';
        }) > -1) {
            isAllPropertyFilled = false;
        }
    }

    if (req.body.id) {
        res.status(400)
        next(new Error('Not to need enter user id'));
    } else if (
        req &&
        req.body &&
        isAllProperty &&
        isAllPropertyFilled) {
        if (
            /([a-zA-Z0-9])*@gmail\.com$/.test(req.body.email) &&
            /\+380([0-9]){9}$/.test(req.body.phoneNumber) &&
            req.body.password.length >= 3 &&
            !req.body.password.includes(' ')
        ) {
            res.status(200);
            next();
        } else {
            res.status(400)
            next(new Error('User entity to update is not valid'));
        }
    } else {
        res.status(400)
        next(new Error('All fields for user entity to update is not filled'));
    }
}

exports.createUserValid = createUserValid;
exports.updateUserValid = updateUserValid;