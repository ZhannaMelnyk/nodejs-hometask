const responseMiddleware = (err, req, res, next) => {
    // TODO: Implement middleware that returns result of the query

    if (res.statusCode == 200) {
        res.status(200);
        next();
    } else {
        const error = JSON.stringify({
            error: true,
            message: err.message
        });

        res.send(error);
    }
}

exports.responseMiddleware = responseMiddleware;