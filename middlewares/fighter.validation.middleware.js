const { fighter } = require('../models/fighter');

const createFighterValid = (req, res, next) => {
    // TODO: Implement validatior for fighter entity during creation

    const bodyKeys = Object.keys(req.body);

    let fighterKeys = Object.keys(fighter);
    fighterKeys.shift();

    let isAllProperty = true;
    let isAllPropertyFilled = true;

    for (let i = 0; i < fighterKeys.length; i++) {
        if (fighterKeys.findIndex(el => {
            return el === bodyKeys[i];
        }) === -1) {
            isAllProperty = false;
        }
    }

    if (isAllProperty) {
        if (fighterKeys.findIndex(el => {
            return req.body[el].trim() === '';
        }) > -1) {
            isAllPropertyFilled = false;
        }
    }

    const numberPattern = new RegExp('^[0-9]*$')

    if (req.body.id) {
        res.status(400)
        next(new Error('Not to need enter fighter id'));
    } else if (
        req &&
        req.body &&
        isAllProperty &&
        isAllPropertyFilled) {
        if (
            numberPattern.test(req.body.health) &&
            numberPattern.test(req.body.power) &&
            numberPattern.test(req.body.defense) &&
            req.body.power < 100 &&
            req.body.defense >= 1 &&
            req.body.defense <= 10
        ) {
            res.status(200);
            next();
        } else {
            res.status(400)
            next(new Error('Fighter entity to create is not valid'));
        }
    } else {
        res.status(400)
        next(new Error('All fields for fighter entity to create is not filled'));
    }
}

const updateFighterValid = (req, res, next) => {
    // TODO: Implement validatior for fighter entity during update
    const bodyKeys = Object.keys(req.body);

    let fighterKeys = Object.keys(fighter);
    fighterKeys.shift();

    let isAllProperty = true;
    let isAllPropertyFilled = true;

    for (let i = 0; i < fighterKeys.length; i++) {
        if (fighterKeys.findIndex(el => {
            return el === bodyKeys[i];
        }) === -1) {
            isAllProperty = false;
        }
    }

    if (isAllProperty) {
        if (fighterKeys.findIndex(el => {
            return req.body[el].trim() === '';
        }) > -1) {
            isAllPropertyFilled = false;
        }
    }

    const numberPattern = new RegExp('^[0-9]*$')

    if (req.body.id) {
        res.status(400)
        next(new Error('Not to need enter fighter id'));
    } else if (
        req &&
        req.body &&
        isAllProperty &&
        isAllPropertyFilled) {
        if (
            numberPattern.test(req.body.health) &&
            numberPattern.test(req.body.power) &&
            numberPattern.test(req.body.defense) &&
            req.body.power < 100 &&
            req.body.defense >= 1 &&
            req.body.defense <= 10
        ) {
            res.status(200);
            next();
        } else {
            res.status(400)
            next(new Error('Fighter entity to update is not valid'));
        }
    } else {
        res.status(400)
        next(new Error('All fields for fighter entity to update is not filled'));
    }
}

exports.createFighterValid = createFighterValid;
exports.updateFighterValid = updateFighterValid;