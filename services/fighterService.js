const { FighterRepository } = require('../repositories/fighterRepository');

class FighterService {
    // TODO: Implement methods to work with fighters
    getFighters() {
        const fighters = FighterRepository.getAll();

        if (!fighters) {
            return null;
        }
        return fighters;
    }

    search(search) {
        const item = FighterRepository.getOne(search);
        if (!item) {
            return null;
        }
        return item;
    }

    createFighter(data) {
        const newFighter = {
            "name": data.name,
            "health": parseInt(data.health),
            "power": parseInt(data.power),
            "defense": parseInt(data.defense)
        }

        const item = FighterRepository.create(newFighter);
        if (!item) {
            return null;
        }
        return item;
    }

    updateFighter(id, dataToUpdate) {
        const newData = {
            "name": dataToUpdate.name,
            "health": parseInt(dataToUpdate.health),
            "power": parseInt(dataToUpdate.power),
            "defense": parseInt(dataToUpdate.defense)
        }
        
        const item = FighterRepository.update(id, newData);
        if(!item) {
            return null;
        }
        return item;
    }

    deleteFighter(id) {
        const item = FighterRepository.delete(id);
        if (!item) {
            return null;
        }
        return item;
    }
}

module.exports = new FighterService();