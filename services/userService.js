const { UserRepository } = require('../repositories/userRepository');

class UserService {

    // TODO: Implement methods to work with user

    getUsers() {
        const users = UserRepository.getAll();
        if (!users) {
            return null;
        }
        return users;
    }

    search(search) {
        const item = UserRepository.getOne(search);
        if (!item) {
            return null;
        }
        return item;
    }

    createUser(data) {
        const newUser = {
            firstName: data.firstName,
            lastName: data.lastName,
            email: data.email,
            phoneNumber: data.phoneNumber,
            password: data.password
        }

        const item = UserRepository.create(newUser);
        if (!item) {
            return null;
        }
        return item;
    }

    updateUser(id, dataToUpdate) {
        const newData = {
            firstName: dataToUpdate.firstName,
            lastName: dataToUpdate.lastName,
            email: dataToUpdate.email,
            phoneNumber: dataToUpdate.phoneNumber,
            password: dataToUpdate.password
        }
        if (UserRepository.getOne(el => el.id === id)) {
            const item = UserRepository.update(id, newData);
            if (!item) {
                return null;
            }
            return item;
        } else {
            return null
        }


    }

    deleteUser(id) {
        const item = UserRepository.delete(id);
        if (!item) {
            return null;
        }
        return item;
    }
}

module.exports = new UserService();