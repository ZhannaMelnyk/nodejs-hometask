const { Router } = require('express');
const FighterService = require('../services/fighterService');
const { responseMiddleware } = require('../middlewares/response.middleware');
const { createFighterValid, updateFighterValid } = require('../middlewares/fighter.validation.middleware');
const path = require("path");

const router = Router();

// TODO: Implement route controllers for fighter
router.get('/', function (req, res, next) {
    const result = FighterService.getFighters(req.body);

    res.send(result);

    next();
})

router.get('/:id', function (req, res, next) {
    const result = FighterService.search(el => el.id === req.params.id);

    if (result) {
        res.send(result);
    } else {
        res.status(404);
        next(new Error('Fighter not found'));
    }
}, responseMiddleware)

router.post('/', createFighterValid, responseMiddleware, function (req, res, next) {
    const data = req.body;

    const result = FighterService.createFighter(data);

    res.send(result);
    next();
})

router.put('/:id', updateFighterValid, function (req, res, next) {
    const data = req.body;

    const result = FighterService.updateFighter(req.params.id, data);
    if (result) {
        res.send(result);
    } else {
        res.status(404);
        next(new Error('User not found'));
    }
    next();
}, responseMiddleware)

router.delete('/:id', function (req, res, next) {
    const result = FighterService.deleteFighter(req.params.id);

    if (result) {
        res.send(result);
    } else {
        res.status(404);
        next(new Error('User not found'));
    }
    next();
}, responseMiddleware)

module.exports = router;