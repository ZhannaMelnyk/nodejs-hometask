const { Router } = require('express');
const UserService = require('../services/userService');
const { createUserValid, updateUserValid } = require('../middlewares/user.validation.middleware');
const { responseMiddleware } = require('../middlewares/response.middleware');

const router = Router();

// TODO: Implement route controllers for user

router.get('/', function (req, res, next) {
    const result = UserService.getUsers();

    res.send(result);
    
    next();
})

router.get('/:id', function (req, res, next) {
    const result = UserService.search(el => el.id === req.params.id);

    if (result) {
        res.send(result);
    } else {
        res.status(404);
        next(new Error('User not found'))
    }
}, responseMiddleware)

router.post('/', createUserValid, responseMiddleware, function (req, res, next) {
    const data = req.body;

    const result = UserService.createUser(data);
    
    res.send(result);
    next();
})

router.put('/:id', updateUserValid, function (req, res, next) {
    const data = req.body;

    const result = UserService.updateUser(req.params.id, data);
    if (result) {
        res.send(result);
    } else {        
        res.status(404);
        next(new Error('User not found'));
    }
    next();
}, responseMiddleware)

router.delete('/:id', function (req, res, next) {
    const result = UserService.deleteUser(req.params.id);

    if (result) {
        res.send(result);
    } else {
        res.status(404);
        next(new Error('User not found'));
    }
    next();
}, responseMiddleware)

module.exports = router;